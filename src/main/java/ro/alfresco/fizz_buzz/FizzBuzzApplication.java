package ro.alfresco.fizz_buzz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FizzBuzzApplication {

    public static void main(String[] args) {
        SpringApplication.run(FizzBuzzApplication.class, args);
        runFizzBuzz(20);
    }

    private static void runFizzBuzz(int input) {
        FizzBuzzConverter fizzBuzzConverter = new FizzBuzzConverter();
        for (int i = 1; i <= input; i++) {
            System.out.print(fizzBuzzConverter.convert(i) + " ");
        }
    }

}
