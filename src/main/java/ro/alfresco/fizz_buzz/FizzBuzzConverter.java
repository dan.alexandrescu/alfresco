package ro.alfresco.fizz_buzz;

public class FizzBuzzConverter {

    public String convert(int toConvert) {
        if (multipleOfThree(toConvert) && multipleOfFive(toConvert)) {
            return "FizzBuzz";
        }

        if (multipleOfFive(toConvert)) {
            return "Buzz";
        }

        if (multipleOfThree(toConvert)) {
            return "Fizz";
        }

        return String.valueOf(toConvert);
    }

    private boolean multipleOfThree(int toVerify) {
        return toVerify % 3 == 0;
    }

    private boolean multipleOfFive(int toVerify) {
        return toVerify % 5 == 0;
    }
}
