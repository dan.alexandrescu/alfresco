package ro.alfresco.fizz_buzz;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FizzBuzzConverterTest {

    FizzBuzzConverter fizzBuzzConverter;

    @BeforeEach
    public void startUp() {
        fizzBuzzConverter = new FizzBuzzConverter();
    }

    @Test
    public void shouldReturnNumberWhenConverterIsCalled() {
        Assertions.assertEquals("1", fizzBuzzConverter.convert(1));
        Assertions.assertEquals("2", fizzBuzzConverter.convert(2));
    }

    @Test
    public void shouldReturnFizzWhenConverterIsCalled() {
        Assertions.assertEquals("Fizz", fizzBuzzConverter.convert(3));
        Assertions.assertEquals("Fizz", fizzBuzzConverter.convert(6));
        Assertions.assertEquals("Fizz", fizzBuzzConverter.convert(9));
    }

    @Test
    public void shouldReturnBuzzWhenConverterIsCalled() {
        Assertions.assertEquals("Buzz", fizzBuzzConverter.convert(5));
        Assertions.assertEquals("Buzz", fizzBuzzConverter.convert(10));
    }

    @Test
    public void shouldReturnFizzBuzzWhenConverterIsCalled() {
        Assertions.assertEquals("FizzBuzz", fizzBuzzConverter.convert(15));
        Assertions.assertEquals("FizzBuzz", fizzBuzzConverter.convert(30));
    }

}
